import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createForms } from 'react-redux-form';
import { DishesReducer } from "./reducers/dishes";
import { CommentsReducer } from "./reducers/comments";
import { PromotionsReducer } from "./reducers/promotions";
import { LeadersReducer } from "./reducers/leaders";
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { initialFeedback } from './forms/feedback-form';

// TODO: FIXME: have this get inferenced
export interface State {
  dishes: ReturnType<typeof DishesReducer>;
  comments: ReturnType<typeof CommentsReducer>;
  promotions: ReturnType<typeof PromotionsReducer>;
  leaders: ReturnType<typeof LeadersReducer>;
  feedback: typeof initialFeedback;
}

export const ConfigureStore = () => {
  const store = createStore(combineReducers<State>({
    dishes: DishesReducer,
    comments: CommentsReducer,
    promotions: PromotionsReducer,
    leaders: LeadersReducer,
    ...createForms({ feedback: initialFeedback })
  }),
  applyMiddleware(thunk, logger));
  
  return store;
}