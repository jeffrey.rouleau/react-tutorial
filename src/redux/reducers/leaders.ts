import { Promotion } from "../../shared/model/promotion";
import {
  ActionTypes,
  AddLeadersAction,
  AddPromotionsAction, LeadersFailedAction, LeadersLoadingAction,
  PromotionsFailedAction,
  PromotionsLoadingAction
} from '../actions/action-types';
import { AsyncProp } from '../../shared/model/async-prop';
import { Leader } from '../../shared/model/leader';

export const LeadersReducer = (
  state: AsyncProp<Leader[]> = null,
  action: AddLeadersAction | LeadersLoadingAction | LeadersFailedAction
) => {
  switch (action.type) {
    case ActionTypes.ADD_LEADERS:
      return [...action.payload];
    case ActionTypes.LEADERS_LOADING:
      return null;
    case ActionTypes.LEADERS_FAILED:
      return action.payload;
    default:
      return state;
  }
}