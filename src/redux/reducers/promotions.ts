import { Promotion } from "../../shared/model/promotion";
import { ActionTypes, AddPromotionsAction, PromotionsFailedAction, PromotionsLoadingAction } from '../actions/action-types';
import { AsyncProp } from '../../shared/model/async-prop';

export const PromotionsReducer = (
  state: AsyncProp<Promotion> = null,
  action: AddPromotionsAction | PromotionsLoadingAction | PromotionsFailedAction
) => {
  switch (action.type) {
    case ActionTypes.ADD_PROMOTIONS:
      return [...action.payload];
    case ActionTypes.PROMOTIONS_LOADING:
      return null;
    case ActionTypes.PROMOTIONS_FAILED:
      return action.payload;
    default:
      return state;
  }
}