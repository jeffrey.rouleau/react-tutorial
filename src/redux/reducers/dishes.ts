import { Dish } from "../../shared/model/dish";
import {
  ActionTypes,
  AddDishesAction,
  DishesFailedAction,
  DishesLoadingAction
} from '../actions/action-types';
import { AsyncProp } from '../../shared/model/async-prop';

export const DishesReducer = (
  state: AsyncProp<Dish[]> = null,
  action: AddDishesAction | DishesLoadingAction | DishesFailedAction
) => {
  switch (action.type) {
    case ActionTypes.ADD_DISHES:
      return [...action.payload]
    case ActionTypes.DISHES_LOADING:
      return null;
    case ActionTypes.DISHES_FAILED:
      return action.payload;
    default:
      return state;
  }
}