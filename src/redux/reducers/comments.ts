import { Comment } from '../../shared/model/comment';
import {
  ActionTypes,
  AddCommentAction,
  AddCommentsAction,
  CommentsFailedAction,
  CommentsLoadingAction
} from '../actions/action-types';
import { AsyncProp } from '../../shared/model/async-prop';

export const CommentsReducer = (
  state: AsyncProp<Comment[]> = null,
  action: CommentsLoadingAction | AddCommentAction | AddCommentsAction | CommentsFailedAction,
) => {
  switch (action.type) {
    case ActionTypes.ADD_COMMENTS:
      return [...action.payload];
    case ActionTypes.COMMENTS_LOADING:
      return null;
    case ActionTypes.COMMENTS_FAILED:
      return action.payload;
    case ActionTypes.ADD_COMMENT:
      const hasData = Array.isArray(state);
      const id = hasData ? state.length : 0;
      const date = new Date().toISOString();
      const comment: Comment = { ...action.payload, id, date };
      return hasData ? [...state, comment] : [comment];
    default:
      return state;
  }
}