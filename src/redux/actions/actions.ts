import {
  ActionTypes,
  AddCommentAction,
  AddCommentsAction,
  AddDishesAction,
  AddLeadersAction,
  AddPromotionsAction,
  CommentsFailedAction,
  CommentsLoadingAction,
  DishesFailedAction,
  DishesLoadingAction,
  FeedbackFailedAction,
  LeadersFailedAction,
  LeadersLoadingAction,
  PromotionsFailedAction,
  PromotionsLoadingAction,
} from './action-types';
import { Action, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { State } from '../configure-store';
import { actions } from 'react-redux-form';
import { baseUrl } from '../../shared/rest/base-url';
import { Dish } from '../../shared/model/dish';
import { Comment } from '../../shared/model/comment';
import { Promotion } from '../../shared/model/promotion';
import { Leader } from '../../shared/model/leader';
import { initialFeedback } from '../forms/feedback-form';

const genError = (response: Response) => 'Error ' + response.status + ': ' + response.statusText;

type ThunkWrapper<T extends Action> = ThunkAction<void, State, undefined, T>;

class ErrorWithResponse extends Error {
  constructor(public message: string, public response: Response) {
    super(message);
  }
}

export const addComment = (comment: Omit<Comment, 'id' | 'date'>): AddCommentAction => ({
  type: ActionTypes.ADD_COMMENT,
  payload: comment,
});

export const postComment = (
  dishId: number,
  rating: number,
  author: string,
  comment: string,
): ThunkAction<Promise<void>, State, undefined, AddCommentAction | CommentsFailedAction> => (
  dispatch
) => {
  const newComment: Omit<Comment, 'id'> = {
    dishId,
    rating,
    author,
    comment,
    date: new Date().toISOString(),
  };
  
  return fetch(`${baseUrl}comments`, {
    method: 'POST',
    body: JSON.stringify(newComment),
    headers: {
      'Content-Type': 'application/json',
    },
    credentials: 'same-origin'
  })
    .then(response => {
        if (response.ok) return response;
        throw  new ErrorWithResponse(genError(response), response);
      },
      error => {
        throw new Error(error.message);
      })
    .then(response => response.json())
    .then(response => {
      dispatch(addComment(response));
    })
    .catch(error => {
      dispatch(commentsFailed(error.message));
    });
}

export const fetchDishes = (): ThunkWrapper<DishesLoadingAction | AddDishesAction | DishesFailedAction> => (
  dispatch
) => {
  dispatch(dishesLoading());
  
  return fetch(`${baseUrl}dishes`)
    .then(response => {
        if (response.ok) return response;
        throw new ErrorWithResponse(genError(response), response);
      },
      error => {
        throw new Error(error.message);
      })
    .then(response => response.json())
    .then(dishes => dispatch(addDishes(dishes)))
    .catch(error => dispatch(dishesFailed(error.message)));
}

export const dishesLoading = (): DishesLoadingAction => ({
  type: ActionTypes.DISHES_LOADING,
});

export const dishesFailed = (err: string): DishesFailedAction => ({
  type: ActionTypes.DISHES_FAILED,
  payload: err,
})

export const addDishes = (dishes: Dish[]): AddDishesAction => ({
  type: ActionTypes.ADD_DISHES,
  payload: dishes,
});

export const resetFeedbackForm = () => actions.reset('feedback');

export const fetchComments = (): ThunkWrapper<CommentsLoadingAction | AddCommentsAction | CommentsFailedAction> => (
  dispatch
) => {
  dispatch(commentsLoading());
  
  return fetch(`${baseUrl}comments`)
    .then(response => {
        if (response.ok) return response;
        throw new ErrorWithResponse(genError(response), response);
      },
      error => {
        throw new Error(error.message);
      })
    .then(response => response.json())
    .then(comments => dispatch(addComments(comments)))
    .catch(error => dispatch(commentsFailed(error.message)));
}

export const commentsLoading = (): CommentsLoadingAction => ({
  type: ActionTypes.COMMENTS_LOADING,
});

export const commentsFailed = (error: string): CommentsFailedAction => ({
  type: ActionTypes.COMMENTS_FAILED,
  payload: error,
});

export const addComments = (comments: Comment[]): AddCommentsAction => ({
  type: ActionTypes.ADD_COMMENTS,
  payload: comments,
});

export const fetchPromotions = (): ThunkWrapper<PromotionsLoadingAction | AddPromotionsAction | PromotionsFailedAction> => (
  dispatch
) => {
  dispatch(promotionsLoading());
  
  return fetch(baseUrl + 'promotions')
    .then(response => {
        if (response.ok) return response;
        throw new ErrorWithResponse(genError(response), response);
      },
      error => {
        throw new Error(error.message);
      })
    .then(response => response.json())
    .then(promotions => dispatch(addPromotions(promotions)))
    .catch(error => dispatch(promotionsFailed(error.message)));
};

export const promotionsLoading = (): PromotionsLoadingAction => ({
  type: ActionTypes.PROMOTIONS_LOADING,
});

export const promotionsFailed = (error: string): PromotionsFailedAction => ({
  type: ActionTypes.PROMOTIONS_FAILED,
  payload: error
});

export const addPromotions = (promotions: Promotion[]): AddPromotionsAction => ({
  type: ActionTypes.ADD_PROMOTIONS,
  payload: promotions
});

export const fetchLeaders = (): ThunkWrapper<LeadersLoadingAction | AddLeadersAction | LeadersFailedAction> => (
  dispatch
) => {
  dispatch(leadersLoading());
  
  return fetch(baseUrl + 'leaders')
    .then(response => {
        if (response.ok) return response;
        throw new ErrorWithResponse(genError(response), response);
      },
      error => {
        throw new Error(error.message);
      })
    .then(response => response.json())
    .then(leaders => dispatch(addLeaders(leaders)))
    .catch(error => dispatch(leadersFailed(error.message)));
};

export const leadersLoading = (): LeadersLoadingAction => ({
  type: ActionTypes.LEADERS_LOADING,
});

export const leadersFailed = (error: string): LeadersFailedAction => ({
  type: ActionTypes.LEADERS_FAILED,
  payload: error
});

export const addLeaders = (leaders: Leader[]): AddLeadersAction => ({
  type: ActionTypes.ADD_LEADERS,
  payload: leaders
});

export const postFeedback = (feedback: typeof initialFeedback): ThunkAction<Promise<void>, State, undefined, FeedbackFailedAction> => (
  dispatch
) => {
  const newFeedback: typeof initialFeedback & { date: string } = {
    ...feedback,
    date: new Date().toISOString(),
  };
  
  return fetch(`${baseUrl}feedback`, {
    method: 'POST',
    body: JSON.stringify(newFeedback),
    headers: {
      'Content-Type': 'application/json',
    },
    credentials: 'same-origin'
  })
    .then(response => {
        if (response.ok) return response;
        throw  new ErrorWithResponse(genError(response), response);
      },
      error => {
        throw new Error(error.message);
      })
    .then(response => response.json())
    .catch(error => {
      dispatch(feedbackFailed(error.message));
    });
}

export const feedbackFailed = (error: string): FeedbackFailedAction => ({
  type: ActionTypes.FEEDBACK_FAILED,
  payload: error
});