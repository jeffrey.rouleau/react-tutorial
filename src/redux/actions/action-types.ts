import { AnyAction } from 'redux';
import { Promotion } from '../../shared/model/promotion';
import { Comment } from '../../shared/model/comment';
import { Dish } from '../../shared/model/dish';
import { Leader } from '../../shared/model/leader';

export enum ActionTypes {
  ADD_COMMENT = 'ADD_COMMENT',
  ADD_COMMENTS = 'ADD_COMMENTS',
  COMMENTS_LOADING = 'COMMENTS_LOADING',
  COMMENTS_FAILED = 'COMMENTS_FAILED',
  PROMOTIONS_LOADING = 'PROMOTIONS_LOADING',
  ADD_PROMOTIONS = 'ADD_PROMOTIONS',
  PROMOTIONS_FAILED = 'PROMOTIONS_FAILED',
  DISHES_LOADING = 'DISHES_LOADING',
  DISHES_FAILED = 'DISHES_FAILED',
  ADD_DISHES = 'ADD_DISHES',
  ADD_LEADERS = 'ADD_LEADERS',
  LEADERS_LOADING = 'LEADERS_LOADING',
  LEADERS_FAILED = 'LEADERS_FAILED',
  FEEDBACK_FAILED = 'FEEDBACK_FAILED',
}

type ActionPayload<K extends keyof typeof ActionTypes, T> = {
  type: K;
  payload: T;
}
export type ActionBase<K extends keyof typeof  ActionTypes, T = null> = T extends null
  ? Omit<ActionPayload<K, T>, 'payload'>
  : ActionPayload<K, T>;

export type AddCommentAction = ActionBase<ActionTypes.ADD_COMMENT, Omit<Comment, 'id' | 'date'>>;

export type DishesLoadingAction = ActionBase<ActionTypes.DISHES_LOADING>;

export type DishesFailedAction = ActionBase<ActionTypes.DISHES_FAILED, string>;

export type AddDishesAction = ActionBase<ActionTypes.ADD_DISHES, Dish[]>;

export type AddCommentsAction = ActionBase<ActionTypes.ADD_COMMENTS, Comment[]>;

export type CommentsLoadingAction = ActionBase<ActionTypes.COMMENTS_LOADING>;

export type CommentsFailedAction = ActionBase<ActionTypes.COMMENTS_FAILED, string>;

export type PromotionsLoadingAction = ActionBase<ActionTypes.PROMOTIONS_LOADING>;

export type AddPromotionsAction = ActionBase<ActionTypes.ADD_PROMOTIONS, Promotion[]>;

export type PromotionsFailedAction = ActionBase<ActionTypes.PROMOTIONS_FAILED, string>;

export type LeadersLoadingAction = ActionBase<ActionTypes.LEADERS_LOADING>;

export type AddLeadersAction = ActionBase<ActionTypes.ADD_LEADERS, Leader[]>;

export type LeadersFailedAction = ActionBase<ActionTypes.LEADERS_FAILED, string>;

export type FeedbackFailedAction = ActionBase<ActionTypes.FEEDBACK_FAILED, string>;

