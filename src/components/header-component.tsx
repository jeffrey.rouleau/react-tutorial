import React, { useRef, useState } from "react";
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavbarToggler,
  Collapse,
  NavItem,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter, Form, FormGroup, Input, Label,
} from "reactstrap";
import { NavLink } from 'react-router-dom';

interface Credentials {
  username: string;
  password: string;
  remember: boolean;
}

const Header = () => {
  const [navOpen, setNavOpen] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [credentials, setCredentials] = useState<Credentials>({
    username: '',
    password: '',
    remember: false,
  })
  
  const setTextInput = (propName: keyof Credentials) =>
    (event: React.ChangeEvent<HTMLInputElement>) => setCredentials(c => ({ ...c, [propName]: event.target.value }));
  
  const clearCredentials = () => {
    setCredentials({
      username: '',
      password: '',
      remember: false
    });
  }
  
  const cancelLogin = (event: React.FormEvent) => {
    setModalOpen(false);
    clearCredentials();
  }
  
  const handleLogin = (event: React.FormEvent) => {
    setModalOpen(false);
    alert(`Username: ${credentials.username} Password: ${credentials.password} remember: ${credentials.remember}`)
    // TODO: forward the credentials on from here.
    event.preventDefault();
    clearCredentials();
  };
  
  return (
    <>
      <Navbar dark expand="md" sticky="top">
        <NavbarToggler onClick={() => setNavOpen(!navOpen)} />
        <NavbarBrand className="mr-auto" href="/">
          <img src="assets/images/logo.png" height="30" alt="Ristorante Con Fusion" />
        </NavbarBrand>
        <Collapse isOpen={navOpen} navbar>
          <Nav navbar>
            <NavItem>
              <NavLink className="nav-link left-align" to="/home">
                <span className="fa fa-home fa-lg center-align" /> Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link left-align" to="/about-us">
                <span className="fa fa-info fa-lg center-align" /> About Us
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link left-align" to="/menu">
                <span className="fa fa-list fa-lg center-align" /> Menu
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link left-align" to="/contact-us">
                <span className="fa fa-address-card fa-lg center-align" /> Contact Us
              </NavLink>
            </NavItem>
          </Nav>
          <Nav className="ms-auto" navbar>
            <NavItem className="left-align">
              <Button outline color="light" onClick={() => setModalOpen(true)}>
                <span className="fa fa-sign-in fa-lg" />Login
              </Button>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
      <div className="jumbotron">
        <div className="row row-header">
          <div className="col-12 col-sm-6 left-align">
            <h1>Ristorante Con Fusion</h1>
            <p>
              We talk inspiration from the World's best cuisines, and create a unique fusion experience.
              Our lipsmacking creations will tickle your culinary senses!
            </p>
          </div>
        </div>
      </div>
      <Modal isOpen={modalOpen} toggle={() => setModalOpen(false)}>
        <ModalHeader toggle={() => setModalOpen(false)}>
          Login
        </ModalHeader>
        <ModalBody>
          <Form onSubmit={handleLogin}>
            <FormGroup>
              <Label htmlFor="username">Username</Label>
              <Input type="text" id="username" name="username" onChange={setTextInput('username')} />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="password">Password</Label>
              <Input type="password" id="password" name="password" onChange={setTextInput('password')} />
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" id="remember" name="remember" onChange={setTextInput('remember')} />
                Remember me
              </Label>
            </FormGroup>
            <ModalFooter>
              <Button type="reset" onClick={cancelLogin}>Cancel</Button>
              <Button
                type="submit"
                value="submit"
                color="primary"
                disabled={!credentials.username || !credentials.password}
              >
                Login
              </Button>
            </ModalFooter>
          </Form>
        </ModalBody>
      </Modal>
    </>
  );
}
export default Header;