import React, { useEffect, useState } from "react";
import Menu from "./pages/menu/menu-component";
import { Routes, Route, Navigate, useParams, useLocation } from 'react-router-dom';
import { connect, MapDispatchToProps } from 'react-redux';
import Header from "./header-component";
import Footer from "./footer-component";
import Home from "./pages/home/home-component";
import ContactUs from "./pages/contact-us/contact-us-component";
import AboutUs from "./pages/about-us/about-us-component";
import DishDetails from "./pages/menu/dish-details-component";
import { Dish } from "../shared/model/dish";
import { Comment } from '../shared/model/comment';
import { State } from "../redux/configure-store";
import {
  fetchDishes as fetchDishesAction,
  fetchComments as fetchCommentsAction,
  resetFeedbackForm as resetFeedbackFormAction,
  fetchPromotions as fetchPromotionsAction,
  fetchLeaders as fetchLeadersAction,
  postComment as postCommentAction,
  postFeedback as postFeedbackAction,
} from "../redux/actions/actions";
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { AsyncProp } from '../shared/model/async-prop';
import { Promotion } from '../shared/model/promotion';
import { Leader } from '../shared/model/leader';
import { NotFound } from '../shared/components/not-found';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { initialFeedback } from '../redux/forms/feedback-form';

type MappedProps = ReturnType<typeof mapStateToProps>;
type MappedActions = ReturnType<typeof mapDispatchToProps>;
// TODO: FIXME: see if I can either simplify back to object mapper or make types better
// type MappedActions = typeof mapDispatchToProps;

const hasDishId = (dishId: string | undefined): dishId is string => {
  return !!dishId?.match(/\d+/);
}

export type PostComment = MappedActions['postComment'];
export type PostFeedback = MappedActions['postFeedback'];
export type ResetFeedbackForm = MappedActions['resetFeedbackForm'];

const DishWithId = ({
  dishes,
  comments,
  postComment
}: {
  dishes: AsyncProp<Dish[]>,
  comments: AsyncProp<Comment[]>,
  postComment: PostComment,
}) => {
  const params = useParams();
  const rawDishId: string | undefined = params.dishId;
  if (!hasDishId(rawDishId)) return <NotFound />;
  const dishId = Number.parseInt(rawDishId, 10);
  const selectedDish = Array.isArray(dishes)
    ? dishes.filter(dish => dish.id === dishId)[0]
    : dishes;
  const selectedComments = Array.isArray(comments)
    ? comments.filter(comment => comment.dishId === dishId)
    : comments;
  return <DishDetails
    dish={selectedDish}
    comments={selectedComments}
    postComment={postComment}
  />;
}

const Main = ({
  dishes,
  featuredDish,
  comments,
  // TODO: FIXME: types look like they are wrong it's got singular and array bleeding through.
  promotions,
  leaders,
  postComment,
  fetchDishes,
  fetchComments,
  fetchPromotions,
  resetFeedbackForm,
  fetchLeaders,
  postFeedback,
}: MappedProps & MappedActions) => {
  useEffect(() => {
    fetchDishes();
    fetchComments();
    fetchPromotions();
    fetchLeaders();
  }, []);
  const featuredPromotion = Array.isArray(promotions)
    ? promotions.filter(promotion => promotion.featured)[0]
    : promotions as AsyncProp<Promotion>
  const HomePage = <Home
    dish={featuredDish}
    promotion={featuredPromotion}
    leader={Array.isArray(leaders) ? leaders.filter(leader => leader.featured)[0] : leaders as AsyncProp<Leader>}
  />;
  const location = useLocation();
  return (
    <div className="page-container">
      <Header />
      <div className="page">
        <TransitionGroup>
          <CSSTransition key={location.key} classNames="page" timeout={300}>
            <Routes>
              <Route path="/home/*" element={HomePage} />
              <Route path="/menu" element={<Menu dishes={dishes} />} />
              <Route path="/menu/:dishId"
                     element={<DishWithId dishes={dishes} comments={comments} postComment={postComment} />} />
              <Route path="/about-us" element={<AboutUs leaders={leaders} />} />
              <Route path="/contact-us"
                     element={<ContactUs resetFeedbackForm={resetFeedbackForm} postFeedback={postFeedback} />} />
              <Route path="*" element={<Navigate to="/home" replace />} />
            </Routes>
          </CSSTransition>
        </TransitionGroup>
      </div>
      <Footer />
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  dishes: state.dishes,
  featuredDish: state.dishes === null
    ? null
    : typeof state.dishes === 'string'
      ? state.dishes
      : state.dishes.filter(dish => dish.featured)[0],
  comments: state.comments,
  promotions: state.promotions,
  leaders: state.leaders,
});

// TODO: FIXME: see if I can either simplify back to object mapper or make types better
// const mapDispatchToProps/*: MapDispatchToProps<{
//   postComment: typeof postCommentAction,
//   fetchDishes: typeof fetchDishesAction,
//   fetchComments: typeof fetchCommentsAction,
//   fetchPromotions: typeof fetchPromotionsAction,
//   resetFeedbackForm: typeof resetFeedbackFormAction,
// }, any>*/ = {
//   postComment: postCommentAction,
//   // fetchDishes: fetchDishesAction,
//   // fetchComments: fetchCommentsAction,
//   // fetchPromotions: fetchPromotionsAction,
//   resetFeedbackForm: resetFeedbackFormAction,
// };

const mapDispatchToProps = (
  dispatch: ThunkDispatch<State, undefined, Action>
) => {
  return {
    postComment: (
      dishId: number,
      rating: number,
      author: string,
      comment: string
    ) => dispatch(postCommentAction(dishId, rating, author, comment)),
    fetchDishes: () => dispatch(fetchDishesAction()),
    fetchComments: () => dispatch(fetchCommentsAction()),
    fetchPromotions: () => dispatch(fetchPromotionsAction()),
    resetFeedbackForm: () => dispatch(resetFeedbackFormAction()),
    fetchLeaders: () => dispatch(fetchLeadersAction()),
    postFeedback: (feedback: typeof initialFeedback) => dispatch(postFeedbackAction(feedback)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);