import { Dish } from '../../../shared/model/dish';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardImg, CardText, CardTitle } from 'reactstrap';
import React from 'react';
import { Link } from 'react-router-dom';
import CommentForm from './comment-form-component';
import { AsyncProp } from '../../../shared/model/async-prop';
import { Comment } from '../../../shared/model/comment';
import { Loading } from '../../../shared/components/loading';
import { baseUrl } from '../../../shared/rest/base-url';
import { ErrorDisplay } from '../../../shared/components/error-display';
import { PostComment } from '../../main-component';
// @ts-ignore TODO: FIXME:
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

const DishDetails = ({
  dish,
  comments,
  postComment,
}: {
  dish: AsyncProp<Dish>,
  comments: AsyncProp<Comment[]>,
  postComment: PostComment,
}) => {
  const renderDish = (dish: Dish) => (
    <FadeTransform in transformProps={{
      exitTransform: 'scale(0.5) translateY(-50%)'
    }}>
      <Card>
        <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name} />
        <CardBody>
          <CardTitle>{dish.name}</CardTitle>
          <CardText>{dish.description}</CardText>
        </CardBody>
      </Card>
    </FadeTransform>
  );
  
  const renderComments = (dishId: number) => (
    <>
      <h1>Comments:</h1>
      {comments === null && <Loading />}
      {(comments === undefined || typeof comments === 'string') && <ErrorDisplay error={comments} />}
        {Array.isArray(comments) &&
          <Stagger in>
            {comments.map(comment => (
              <Fade in key={comment.id}>
                <p>
                  <span>{comment.comment}</span>
                  <br />
                  <span className="text-muted comment-info">  -- {
                    comment.author
                  }, {new Intl.DateTimeFormat(
                    'en-US',
                    {
                      year: 'numeric',
                      month: 'short',
                      day: '2-digit',
                    },
                  ).format(new Date(comment.date))}</span>
                </p>
              </Fade>
          ))}
        </Stagger>
      }
      <CommentForm dishId={dishId} postComment={postComment} />
    </>
  );
  
  if (dish === null || typeof dish === 'boolean') return <Loading />;
  else if (dish === undefined || typeof dish === 'string')  return <ErrorDisplay error={dish} />;
  else return (
    <div className="container">
      <div className="row">
        <Breadcrumb>
          <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
          <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
          <BreadcrumbItem active>{dish.name}</BreadcrumbItem>
        </Breadcrumb>
      </div>
      <div className="row">
        <div className="col-12 col-md-5 m-1">
          {renderDish(dish)}
        </div>
        <div className="col-12 col-md-5 m-1">
          {renderComments(dish.id)}
        </div>
      </div>
    </div>
  );
}
export default DishDetails;