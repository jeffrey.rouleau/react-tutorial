import { connect } from 'react-redux';
import {
  Button,
  Col,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row
} from 'reactstrap';
import React, { useState } from 'react';
import { Control, Errors, LocalForm } from "react-redux-form";
import { maxLength, minLength, required } from "../../../shared/validators/form-validators";
import { State } from "../../../redux/configure-store";
import { PostComment } from '../../main-component';

const CommentForm = ({ dishId, postComment }: { dishId: number, postComment: PostComment }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [formInvalid, setFormInvalid] = useState(true);
  
  const cancelCommenting = (event: React.FormEvent) => {
    setModalOpen(false);
  }
  
  const handleUpdate = (form: { $form: { valid: boolean, pristine: boolean }}) => {
    setFormInvalid(!form.$form.valid || form.$form.pristine);
  }
  
  const handleSubmit = (values: { rating: number, author: string, comment: string }) => {
    postComment(dishId, values.rating, values.author, values.comment);
    setModalOpen(false);
  }
  
  return (
    <>
      <Button outline onClick={() => setModalOpen(true)}>
        <span className="fa fa-pencil fa-lg me-2"></span>
        Leave a Comment
      </Button>
      <Modal isOpen={modalOpen} toggle={() => setModalOpen(false)}>
        <ModalHeader toggle={() => setModalOpen(false)}>
          Leave a Comment
        </ModalHeader>
        <ModalBody>
          <LocalForm
            onUpdate={form => handleUpdate(form)}
            onSubmit={values => handleSubmit(values)}>
            <Row className="mb-3">
              <Label htmlFor="rating" md={2}>Rating</Label>
              <Col md={10}>
                <Control.select
                  name="rating"
                  model=".rating"
                  className="form-select"
                >
                  {Array(5).fill(0).map((_, i) => <option key={i}>{i + 1}</option>)}
                </Control.select>
              </Col>
            </Row>
            <Row className="mb-3">
              <Label htmlFor="rating" md={2}>Your Name</Label>
              <Col md={10} className="my-auto">
                <Control.text
                  id="author"
                  name="author"
                  model=".author"
                  placeholder="Your Name"
                  className="form-control"
                  validators={{
                    required,
                    minLength: minLength(3),
                    maxLength: maxLength(15),
                  }}
                />
                <Errors
                  className="text-danger errors"
                  model=".author"
                  show="touched"
                  messages={{
                    required: 'Required',
                    minLength: 'Must be greater than 2 characters',
                    maxLength: 'Must be 15 characters or less',
                  }}
                />
              </Col>
            </Row>
            <Row className="mb-3">
              <Label htmlFor="comment" md={2}>Comment</Label>
              <Col md={10}>
                <Control.textarea
                  id="comment"
                  name="comment"
                  model=".comment"
                  rows={6}
                  className="form-control"
                  validators={{
                    required,
                  }}
                />
                <Errors
                  className="text-danger errors"
                  model=".comment"
                  show="touched"
                  messages={{
                    required: 'Required',
                  }}
                />
              </Col>
            </Row>
            <ModalFooter>
              <Button type="reset" onClick={cancelCommenting}>Cancel</Button>
              <Button
                type="submit"
                value="submit"
                color="primary"
                disabled={formInvalid}
              >
                Submit
              </Button>
            </ModalFooter>
          </LocalForm>
        </ModalBody>
      </Modal>
    </>
  );
};

const mapStateToProps = (state: State) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CommentForm);