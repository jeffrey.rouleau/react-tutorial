import React from 'react';
import {
  Card,
  CardImg,
  CardImgOverlay,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';
import { Dish } from "../../../shared/model/dish";
import { Link } from "react-router-dom";
import { Loading } from '../../../shared/components/loading';
import { AsyncProp } from '../../../shared/model/async-prop';
import { baseUrl } from '../../../shared/rest/base-url';
import { ErrorDisplay } from '../../../shared/components/error-display';

type DishesProp = AsyncProp<Dish[]>;


const RenderDish = ({
  dish,
}: {
  dish: Dish;
}) => (
  <div key={dish.id} className="col-12 col-md-5 m-1">
    <Card>
      <Link to={`/menu/${dish.id}`}>
        <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name} />
        <CardImgOverlay>
          <CardTitle className="dish-title">{dish.name}</CardTitle>
        </CardImgOverlay>
      </Link>
    </Card>
  </div>
);

const genMenus = (dishes: DishesProp) => {
  if (dishes === null || typeof dishes === 'boolean') return <Loading />;
  else if (dishes === undefined || typeof dishes === 'string') return <ErrorDisplay error={dishes} />
  else return dishes.map(dish => <RenderDish key={dish.id} dish={dish} />);
}
const Menu = ({
  dishes,
}: {
  dishes: DishesProp,
}) => (
  <div className="container">
    <div className="row">
      <Breadcrumb>
        <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
        <BreadcrumbItem active>Menu</BreadcrumbItem>
      </Breadcrumb>
      <div className="col-12">
        <h3>Menu</h3>
        <hr />
      </div>
    </div>
    <div className="row center-content">
      {genMenus(dishes)}
    </div>
  </div>
);
export default Menu;