import React from 'react';
import { Dish } from "../../../shared/model/dish";
import { Promotion } from "../../../shared/model/promotion";
import { Leader } from "../../../shared/model/leader";
import RenderCard from "../../../shared/components/render-card";
import { AsyncProp } from '../../../shared/model/async-prop';
// @ts-ignore TODO: FIXME:
import { FadeTransform } from 'react-animation-components';

const Home = ({
  dish,
  promotion,
  leader,
}: {
  dish: AsyncProp<Dish>,
  promotion: AsyncProp<Promotion>,
  leader: AsyncProp<Leader>,
}) => {
  return (
    <div className="container">
      <div className="row align-items-start full-width">
        <div className="col-12 col-md m-1">
          <FadeTransform in transformProps={{
            exitTransform: 'scale(0.5) translateY(-50%)'
          }}>
            <RenderCard item={dish} />
          </FadeTransform>
        </div>
        <div className="col-12 col-md m-1">
          <FadeTransform in transformProps={{
            exitTransform: 'scale(0.5) translateY(-50%)'
          }}>
            <RenderCard item={promotion} />
          </FadeTransform>
        </div>
        <div className="col-12 col-md m-1">
          <FadeTransform in transformProps={{
            exitTransform: 'scale(0.5) translateY(-50%)'
          }}>
            <RenderCard item={leader} />
          </FadeTransform>
        </div>
      </div>
    </div>
  );
}
export default Home;