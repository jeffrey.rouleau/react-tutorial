import React from 'react';
import { Breadcrumb, BreadcrumbItem, Button, Col, Label, Row } from 'reactstrap';
import { Control, LocalForm, Errors, Form } from 'react-redux-form';
import { Link } from 'react-router-dom';
import {
  maxLength,
  minLength,
  required,
  isNumber,
  validEmail,
} from '../../../shared/validators/form-validators';
import { initialFeedback } from '../../../redux/forms/feedback-form';
import { PostFeedback, ResetFeedbackForm } from '../../main-component';

enum ContactType {
  Tel = 'Tel.',
  Email = 'Email',
}

const ContactUs = ({
  resetFeedbackForm,
  postFeedback,
}: { resetFeedbackForm: ResetFeedbackForm, postFeedback: PostFeedback  }) => {
  const handleSubmit = (values: typeof initialFeedback) => {
    alert(`Current State is: ${JSON.stringify(values, undefined, 2)}`);
    postFeedback(values);
    resetFeedbackForm();
  }
  
  return (
    <div className="container">
      <div className="row">
        <Breadcrumb>
          <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
          <BreadcrumbItem active>Contact Us</BreadcrumbItem>
        </Breadcrumb>
        <div className="col-12">
          <h3>Contact Us</h3>
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <h3>Location Information</h3>
        </div>
        <div className="col-12 col-sm-4 offset-sm-1">
          <h5>Our Address</h5>
          <address>
            121, Clear Water Bay Road<br />
            Clear Water Bay, Kowloon<br />
            HONG KONG<br />
            <i className="fa fa-phone"></i>: +852 1234 5678<br />
            <i className="fa fa-fax"></i>: +852 8765 4321<br />
            <i className="fa fa-envelope"></i>: <a href="mailto:confusion@food.net">confusion@food.net</a>
          </address>
        </div>
        <div className="col-12 col-sm-6 offset-sm-1">
          <h5>Map of our Location</h5>
        </div>
        <div className="col-12 col-sm-12">
          <div className="btn-group" role="group">
            <a role="button" className="btn btn-primary" href="tel:+85212345678"><i
              className="fa fa-phone"></i> Call</a>
            <a role="button" className="btn btn-info"><i className="fa fa-skype"></i> Skype</a>
            <a role="button" className="btn btn-success" href="mailto:confusion@food.net"><i
              className="fa fa-envelope-o"></i> Email</a>
          </div>
        </div>
      </div>
      <div className="row row-content">
        <div className="col-12">
          <h3>Send us your feedback</h3>
          <div className="col-12">
            <Form model="feedback" onSubmit={values => handleSubmit(values)}>
              <Row className="mb-3">
                <Label htmlFor="firstName" md={2}>First Name</Label>
                <Col md={10} className="my-auto">
                  <Control.text
                    id="firstName"
                    name="firstName"
                    model=".firstName"
                    placeholder="First Name"
                    className="form-control"
                    validators={{
                      required,
                      minLength: minLength(3),
                      maxLength: maxLength(15),
                    }}
                  />
                  <Errors
                    className="text-danger errors"
                    model=".firstName"
                    show="touched"
                    messages={{
                      required: 'Required',
                      minLength: 'Must be greater than 2 characters',
                      maxLength: 'Must be 15 characters or less',
                    }}
                  />
                </Col>
              </Row>
              <Row className="mb-3">
                <Label htmlFor="lastName" md={2}>Last Name</Label>
                <Col md={10} className="my-auto">
                  <Control.text
                    id="lastName"
                    name="lastName"
                    model=".lastName"
                    placeholder="Last Name"
                    className="form-control"
                    validators={{
                      required,
                      minLength: minLength(3),
                      maxLength: maxLength(15),
                    }}
                  />
                  <Errors
                    className="text-danger errors"
                    model=".lastName"
                    show="touched"
                    messages={{
                      required: 'Required',
                      minLength: 'Must be greater than 2 characters',
                      maxLength: 'Must be 15 characters or less',
                    }}
                  />
                </Col>
              </Row>
              <Row className="mb-3">
                <Label htmlFor="telNum" md={2}>Telephone Number</Label>
                <Col md={10} className="my-auto">
                  <Control.text
                    id="telNum"
                    name="telNum"
                    model=".telNum"
                    placeholder="Telephone Number"
                    className="form-control"
                    validators={{
                      required,
                      minLength: minLength(3),
                      maxLength: maxLength(15),
                      isNumber,
                    }}
                  />
                  <Errors
                    className="text-danger errors"
                    model=".telNum"
                    show="touched"
                    messages={{
                      required: 'Required',
                      minLength: 'Must be greater than 2 characters',
                      maxLength: 'Must be 15 characters or less',
                      isNumber: 'Must be a number',
                    }}
                  />
                </Col>
              </Row>
              <Row className="mb-3">
                <Label htmlFor="email" md={2}>Email</Label>
                <Col md={10} className="my-auto">
                  <Control.text
                    id="email"
                    name="email"
                    model=".email"
                    placeholder="Email"
                    className="form-control"
                    validators={{
                      required,
                      validEmail,
                    }}
                  />
                  <Errors
                    className="text-danger errors"
                    model=".email"
                    show="touched"
                    messages={{
                      required: 'Required',
                      validEmail: 'Invalid email address',
                    }}
                  />
                </Col>
              </Row>
              <Row className="mb-3">
                <Col md={{ size: 6, offset: 2 }} className="my-auto">
                  <div className="form-check my-auto">
                    <Label check className="my-auto">
                      <Control.checkbox
                        name="agree"
                        model=".agree"
                        className="form-check-input"
                      />
                      {' '}
                      <strong>May we contact you?</strong>
                    </Label>
                  </div>
                </Col>
                <Col md={{ size: 3, offset: 1 }}>
                  <Control.select
                    name="contactType"
                    model=".contactType"
                    className="form-select"
                  >
                    <option>{ContactType.Tel}</option>
                    <option>{ContactType.Email}</option>
                  </Control.select>
                </Col>
              </Row>
              <Row className="mb-3">
                <Label htmlFor="message" md={2}>Your Feedback</Label>
                <Col md={10}>
                  <Control.textarea
                    id="message"
                    name="message"
                    model=".message"
                    rows={12}
                    className="form-control"
                  />
                </Col>
              </Row>
              <Row className="mb-3">
                <Col md={{ size: 10, offset: 1 }}>
                  <Button type="submit" color="primary">
                    Send Feedback
                  </Button>
                </Col>
              </Row>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
export default ContactUs;