// TODO: fIXME: need to not make it any and support optional properties using Record
export type AsyncProp<T extends { [key: string | number | symbol]: any } | Array<unknown>> = T | null | string;