import { Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle } from "reactstrap";
import React, { ReactHTMLElement } from "react";
import { Loading } from './loading';
import { AsyncProp } from '../model/async-prop';
import { baseUrl } from '../rest/base-url';
import { ErrorDisplay } from './error-display';

export const LoadingCard = () => (
  <div className="card placeholder-item placeholder-card" />
);

const RenderCard = ({
  item
}: {
  item: AsyncProp<{
    image: string;
    name: string;
    designation?: string;
    description: string;
  }>,
}) => {
  if (item === null) return <LoadingCard />;
  else if (item === undefined || typeof item === 'string') return <ErrorDisplay error={item} />;
  else return (
    <Card>
      <CardImg src={baseUrl + item.image} alt={item.name} />
      <CardBody>
        <CardTitle>{item.name}</CardTitle>
        {item.designation && <CardSubtitle>{item.designation}</CardSubtitle>}
        <CardText>{item.description}</CardText>
      </CardBody>
    </Card>
  );
}
export default RenderCard;