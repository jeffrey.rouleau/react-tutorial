import React from 'react';

export const ErrorDisplay = ({ error }: { error: string | undefined }) => (
  <div className="col-12 text-danger">
    <p>error!{error && `: ${error}`}</p>;
  </div>
);