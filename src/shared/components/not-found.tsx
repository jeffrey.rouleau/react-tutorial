import React from 'react';

export const NotFound = () => (
  <div className="row col-12 align-items-center">
    <div className="col-1 offset-5 text-center">
      <span className="display-1 text-nowrap">:(</span>
    </div>
    <div className="col-2">
      <h3>Item not found!</h3>
    </div>
  </div>
);